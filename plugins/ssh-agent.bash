#!/bin/bash

# Get the ssh environment
source ~/.ssh/agent_env >/dev/null 2>/dev/null
# See if ssh-agent is running and connected
ssh-add -l >/dev/null 2>/dev/null
if [[ $? -eq 2 ]]; then
    echo "Starting new ssh-agent"
    ssh-agent -s >|~/.ssh/agent_env
    # source the newly provisioned agent
    source ~/.ssh/agent_env
fi

# Test if ssh-agent has any keys loaded
ssh-add -l >/dev/null 2>/dev/null
if [[ $? -eq 1 ]]; then
    echo "Loading ssh-keys into ssh-agent"
    for key in ~/.ssh/{*rsa,*key,*ed25519}; do
        ssh-add $key
    done
fi
