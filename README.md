# bash.d
bash configs and scripts

## Quick-start
1. `git clone git@git.soma.salesforce.com:theodore-davis/bash.d.git ~/.bash.d`
2. `bash ~/.bash.d/bin/setup.sh`
3. `exec bash -l` or `source ~/.bash_profile`

## Features
- plugins
- functions
- configuration
- bash-completion
- comes with sensible.bash turned on by default

### Plugins
#### [sensible bash](https://github.com/mrzool/bash-sensible)
#### [ssh-agent](https://gitlab.com/teddavis4/bash.d/blob/master/docs/ssh-agent.md)
#### [pyenv](https://gitlab.com/teddavis4/bash.d/blob/master/docs/pyenv.md)

### Functions
- [aws](https://gitlab.com/teddavis4/bash.d/blob/master/docs/aws.md)
- [bash](https://gitlab.com/teddavis4/bash.d/blob/master/docs/bash.md)
- [dir](https://gitlab.com/teddavis4/bash.d/blob/master/docs/dir.md)
- [docker](https://gitlab.com/teddavis4/bash.d/blob/master/docs/docker.md)
- [git](https://gitlab.com/teddavis4/bash.d/blob/master/docs/git.md)
- [ls](https://gitlab.com/teddavis4/bash.d/blob/master/docs/ls.md)
- [tmux](https://gitlab.com/teddavis4/bash.d/blob/master/docs/tmux.md)
- [vim](https://gitlab.com/teddavis4/bash.d/blob/master/docs/vim.md)

### Configuration
bash.d does not try to provide too much by default, however, with that being said bash.d *does* come with sensible.bash baked in by default. This is easily removed by removing or renaming the file in `plugins/sensible.bash`.  
#### Configuration options
- `BASH_D_DIR` -- where bash.d is installed (default: $HOME/.bash.d)
- `BASH_D_VERBOSE` -- log level (1-5) of bash.d logs (default: 5)
- `BASH_D_LOG_DIR` -- where to put logs (default: $HOME/.log)
#### Some things bash.d currently provides by default
- Prompt/PS1
    - bash.d provides a dynamic prompt including
    - hostname
        - computer model if on mac (MacPro, MacBookPro, MacBook)
    - current directory (short style)
    - username
    - git status (git-repo and status)
- git PS1 modifiers
    - Show dirty state (%)
    - Show untracked files (\*)
    - Show stash state ($)
    - Use colors
- bash completion
    - bash.d looks for completions in `/usr/local/etc/bash_completion.d` for system-wide completion files
    - bash loads the system wide completion from `/usr/local/etc/profile.d/bash_completion.sh` (only if it exists)
