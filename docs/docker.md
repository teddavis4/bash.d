## Docker helper functions
these provide some useful shortcuts to common docker tasks

#### Functions
- `stop_docker` -- stops the docker daemon on macOS
- `start_docker` -- starts docker on macOS
- `docker_stop_containers` -- top all running containers
