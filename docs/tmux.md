## Tmux helpers
tmux can do a lot, and sometimes it's hard to remember all those things. tmux.bash to the rescue! tmux.bash has functions to help perform common tmux tasks

#### Functions
- `tm` -- try to connect to a session named 'main' otherwise create one
