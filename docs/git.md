## Git helpers
git.bash provides functions to help shortcut common tasks like rebasing or merging from master and pulling the latest bits.

#### Functions
- `bsync` -- checks out origin/master then if an upstream exists it rebased upstream/master into HEAD then pushes any changes to remote/origin/master otherwise it just pulls latest bits from origin/master
- `rebase` -- uses `bsync` to pull the latest bits to origin/master then rebases the current branch over origin/master
    - argument: `-s` -- do git stash first (default: no)
- `merge` -- uses `bsync` to pull the latest bits to origin/master then merges origin/master into the current branch
    - argument: `-s` -- do git stash first (default: no)
