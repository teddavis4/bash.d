## AWS nd helpers
The most useful functions provided by aws.bash is `get_mfa_creds` and `reset_git_credentials`.  `get_mfa_creds` will retrieve a security token using MFA and cache the credentials for 12 hours. `reset_git_credentials` allows you to clear the keychain value for the git credentials provided by the aws git credential helper.

### Functions
- `get_aws_account_id` -- get the aws account ID from the current credentials
- `get_aws_user_name` -- get the username from the current credentials
- `get_mfa_creds` -- gets a security token from an MFA device
- `reset_git_credentials` -- reset the git credentials in the OSX keychain
- `get_cached_aws_credentials` -- get credentials from the cache file
- `get_new_aws_credentials` -- get new credentials using a MFA token code and cache them
