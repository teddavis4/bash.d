## General functions to augment the shell
bash.bash provides functions that help writing cleaner functions or oneliners.

#### Functions
- `silent` -- ignore all output
- `quiet` -- ignore stdout
- `happy` -- ignore stderr
- `noerr` -- pipe stderr to stdout
- `noout` -- pipe stdout to stderr
- `failed` -- wrap text in a red banner
- `success` -- wrap text in a green banner
- `reload` -- reloads bash
