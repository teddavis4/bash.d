## Features
- add clicolors
    - supports macOS and Linux
## Aliases/functions
- `l` -- `ls -loghF`
- `ll` -- `ls -l`
- `lrt` -- `ls -alhrt` -- get all files, sort them by modification time and give human readable sizes
