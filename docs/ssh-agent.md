## ssh-agent
automatically manage ssh-agents and ssh-keys in your shell.

#### Features
ssh-agent.bash checks for an existing ssh-agent session and attaches to it if available. If not session is available then it creates a new one and stores the configuration in ~/.ssh for other shell instances to utilize.

#### Quick-start
just create an ssh key file using `ssh-keygen` and name it with a suffix of `['rsa', 'key', 'ed25519']`
