#!/usr/bin/env bash

vim_usage() {
    cat <<EOF
This function requires a vim docker image
EOF
}

check_for_vim_container() {
    docker images | grep -q vim
}

v() {
    check_for_vim_container || vim_usage
    local ftype=$(file -b ${1:-.})
    if [[ $ftype == 'directory' ]]; then
        if [[ ${ftype:0:1} == '/' ]]; then
            docker run -it --rm -v $1:/project -v $HOME/.backup:/root/.backup vim
        else
            docker run -it --rm -v ${PWD}/${1}:/project -v $HOME/.backup:/root/.backup vim
        fi
    else
        if [[ ${ftype:0:1} == '/' ]]; then
            docker run -it --rm -v $(dirname $1):/project -v $HOME/.backup:/root/.backup vim $(basename $1)
        else
            docker run -it --rm -v ${PWD}/$(dirname $1):/project -v $HOME/.backup:/root/.backup vim $(basename $1)
        fi
    fi
}

if [[ ${REPLACE_VIM_WITH_NVIM:-0} -gt 0 ]]; then
    alias vim=nvim
fi
