function bu() {
    brew update
}

function bug() {
    bu
    brew upgrade
}

function bo() {
    brew outdated
}

function boc() {
    brew outdated --cask --greedy
}

function buc() {
    bu
    local casks=$(brew outdated --quiet --cask --greedy)
    for cask in ${casks}; do
        echo "Upgrading cask: ${cask}"
        brew upgrade --cask --force "${cask}"
    done
}
