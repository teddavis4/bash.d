declare -A alias_to_dir
CONFIG_FILE="${BASH_D_DIR}/config/dir.bash"
touch "${CONFIG_FILE}"

D_HELP='\td -- help
-h, --help ...... show help
-a, --add ....... add dir
-c, --current ... add current dir
-d, --delete .... delete dir
-l, --list ...... list dirs (default if no arguments provided)
-r, --reload .... reload aliases
<dir> ........... dir to navigate to'
function print_d_help() {
    echo -e "${D_HELP}"
}
function take() {
    mkdir -p "$@"
    cd "$@" || exit
}

# Directory alias (for easy navigation)
function d() {
    local dir
    local alias
    while [[ $# -gt 0 ]]; do
        case $1 in
        -h | --help)
            print_d_help
            return 0
            ;;
        -a | --add)
            alias="$2"
            dir="$3"
            alias_dir "${alias}" "${dir}"
            return 0
            ;;
        -c | --current)
            alias="$2"
            dir="$PWD"
            alias_dir "${alias}" "${dir}"
            return 0
            ;;
        -d | --delete | --del)
            alias="$2"
            unalias_dir "${alias}"
            return 0
            ;;
        -l | --list)
            _list_aliases
            return 0
            ;;
        -r | --reload)
            _reload_aliases
            return 0
            ;;
        -* | --*)
            echo "Unknown option $1"
            return 255
            ;;
        *)
            go_to_dir "${1}"
            return 0
            ;;
        esac
    done
    _list_aliases
}

function __d() {
    # COMPREPLY=($(compgen -W $(for _c in "${!alias_to_dir[@]}"; do echo "${_c}"; done) "${COMP_WORDS[1]}"))
    COMPREPLY=($(compgen -W "${!alias_to_dir[*]}" "${COMP_WORDS[1]}"))
}
complete -F __d d

function alias_dir() {
    local alias=${1}
    local dir=${2}
    _list_aliases | grep "${dir}$" && {
        echo "dir already exists"
        return 201
    }
    echo "adding alias_to_dir[${alias}]=\"${dir}\""
    alias_to_dir[${alias}]="${dir}"
    _write_aliased_dirs
}

function unalias_dir() {
    local alias=${1}
    echo "removing alias_to_dir[${alias}]=\"${alias_to_dir[${alias}]}\""
    unset alias_to_dir["${alias}"]
    _write_aliased_dirs
}

function acd() {
    alias_dir "${1}" "${PWD}"
}

function _write_aliased_dirs() {
    (
        for aliased_dir in "${!alias_to_dir[@]}"; do
            echo "alias_to_dir[${aliased_dir}]=\"${alias_to_dir[${aliased_dir}]}\""
        done
    ) >|"${CONFIG_FILE}"
}

function _list_aliases() {
    for aliased_dir in "${!alias_to_dir[@]}"; do
        echo "${aliased_dir} ->> ${alias_to_dir[${aliased_dir}]}"
    done
    return 0
}

function _reload_aliases() {
    # shellcheck source=/dev/null
    source "${CONFIG_FILE}"
}

function go_to_dir() {
    local alias_to_go="${1}"
    for aliased_dir in "${!alias_to_dir[@]}"; do
        if [[ ${alias_to_go} == "${aliased_dir}" ]]; then
            local dir_to_go="${alias_to_dir[${alias_to_go}]}"
            log_debug "navigating to ${alias_to_go} ->> ${dir_to_go}"
            cd "${dir_to_go}" || exit
            return
        fi
    done
    echo "could not find alias for ${alias_to_go}"
}

_reload_aliases
