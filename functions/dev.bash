#!/usr/bin/env bash

function get_todo() {
    echo "TODO:$(openssl rand -hex 6)" | tr -d '\n' | pbcopy
}

function senile() (
    TOPLEVEL=$(git rev-parse --show-toplevel)
    git diff --name-only | xargs -I% grep -Hln SENILE "${TOPLEVEL}"/%
)
