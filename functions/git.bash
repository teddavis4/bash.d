#!/usr/bin/env bash

function check_for_upstream() {
    git remote | grep -q upstream
}

function bsync() {
    if [[ $1 == "-v" ]]; then
        set -x
    fi
    # Checkout ${MAIN_BRANCH_NAME} and rebase upstream
    {
        local develop_branch="${MAIN_BRANCH_NAME:-develop}"
        silent git checkout "${develop_branch}"
        silent git pull
        silent check_for_upstream && silent git pull upstream "${develop_branch}"
        silent git push origin "${develop_branch}"
    } && success "successfully synced ${develop_branch}" || failed "FAILED"
    if [[ $1 == "-v" ]]; then
        set +x
    fi
}

function rup() {
    local branch
    branch=$(cb)

    git stash
    bsync
    git checkout "${branch}"
    git stash pop

}

function rebase() {
    local rebase_args=()
    local stash=
    while (($#)); do
    case $1 in
        -s | --stash)
            stash=true
            silent git stash
            shift
            ;;
        *)
            rebase_args+=("$1")
            shift
            ;;
    esac
    done
    if [[ $1 == "-s" ]]; then
        silent git stash
    fi
    {
        local develop_branch
        local cur_branch
        develop_branch="origin/${MAIN_BRANCH_NAME:-develop}"
        cur_branch=$(git rev-parse --abbrev-ref HEAD)
        silent bsync || failed "failed to run bsync"
        silent git checkout "$cur_branch"
        silent git rebase "${rebase_args[@]}" "$develop_branch"
    } &&
        success "successfully rebased against $develop_branch" ||
        failed "FAILED"
    if [[ -n ${stash:-} ]]; then
        silent git stash pop
    fi
}

function merge() {
    if [[ $1 == "-s" ]]; then
        silent git stash
    fi
    {
        local develop_branch
        local cur_branch
        develop_branch="origin/${MAIN_BRANCH_NAME:-develop}"
        cur_branch=$(git rev-parse --abbrev-ref HEAD)
        silent bsync || failed "failed to run bsync"
        silent git checkout "$cur_branch"
        silent git merge "$develop_branch"
    } &&
        success "successfully merged from $develop_branch" ||
        failed "FAILED"
    if [[ $1 == "-s" ]]; then
        silent git stash pop
    fi
}

function _get_formatting_tool() {
    declare -A tools
    tools[tf]='sc terraform fmt'
    tools[bash]='shfmt -s -w -i 4'
    echo >&2 "ext: $1, tool: ${tools[$1]}"
    echo "${tools[$1]}"
}

function gitfmt() (
    local toplevel
    local files
    local ext
    toplevel="$(git rev-parse --show-toplevel)"
    cd "$toplevel" || return 255
    files=$(git diff --name-only "${MAIN_BRANCH_NAME:-develop}" | sort | uniq)

    for file in $files; do
        ext=$(echo $file | rev | cut -d'.' -f1 | rev)
        formatter=$(_get_formatting_tool $ext)
        $formatter "$file"
    done

)

if [[ $BASH_D_GIT != "" ]]; then
    GIT=$(type -P git)
    log_debug "loading GIT from ${GIT}"
    function commit() (
        cd "$($GIT rev-parse --show-toplevel)" || {
            echo "can't find top level..."
            return
        }
        local files_changed
        files_changed=$($GIT diff --staged --name-only | uniq)
        files_changed="${files_changed} $($GIT diff --name-only | uniq)"
        if grep -qHnr "[S]ENILE" "${files_changed[@]:-/dev/null}"; then
            echo "S""ENILE COMMIT"
        else
            $GIT commit -v "$@"
        fi
    )

    function git() {
        if [[ $1 == "commit" ]]; then
            shift
            commit "$@"
        else
            $GIT "$@"
        fi
    }
fi

#alias nb='bsync && git checkout -b'
function nb() {
	local new_branch=$(echo "$@" | tr ' ' '-' | sed 's/[!@#$%^&*()]/-/g' | tr "'" "-" | tr '"' '-' | tr '[A-Z]' '[a-z]')
	new_branch="theodore/${new_branch}"
	echo "creating new branch: ${new_branch}"
	git checkout -b "${new_branch}"
}

# shellcheck disable=SC2142
alias delete_dead_branches='git branch -vv | grep '\'': gone]'\'' | awk '\''{print $1}'\'' | xargs git branch -D'
alias cb='git branch --show-current'
alias ccb='cb | pbcopy'
alias gs='git status'
alias gb='git branch'
alias gc='git commit'
alias gca='git commit -a'
alias gd='git diff'
alias gp='git push'
