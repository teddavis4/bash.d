#!/usr/bin/env bash

function get_aws_account_id() {
    export aws_account_id=$($aws_command sts get-caller-identity | jq -r '.Account' || echo)
}
function get_aws_user_name() {
    export aws_user_name=$($aws_command sts get-caller-identity | jq -r '.Arn' | rev | cut -d '/' -f1 | rev)
}

function get_mfa_creds() {
    profile=${1:-default}
    aws_command="aws --profile ${profile:-default}"
    get_aws_account_id
    get_aws_user_name
    local token_code=$1
    unset AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN AWS_ACCESS_KEY_ID
    get_cached_aws_credentials || get_new_aws_credentials $token_code
}

function reset_git_credentials() {
    while :; do
        security find-internet-password -s git-codecommit.us-west-2.amazonaws.com ||
            break &&
            security delete-internet-password -s git-codecommit.us-west-2.amazonaws.com
    done 2>/dev/null >/dev/null
}

function get_cached_aws_credentials() {
    local cache_date=$(head -n1 $BASH_D_DIR/cache/aws_session_$profile | awk -F "::" '{print $2}')
    if [[ $(date +%s) -lt $cache_date ]]; then
        source $BASH_D_DIR/cache/aws_session_$profile
    else
        echo "Your creds expired, provide a token: "
        read token
        get_new_aws_credentials $token
    fi
}

function get_new_aws_credentials() {
    local token_code=$1
    aws_creds=$($aws_command sts get-session-token --serial-number "arn:aws:iam::${aws_account_id}:mfa/$aws_user_name" --token-code ${token_code})
    cat <<EOF >|$BASH_D_DIR/cache/aws_session_$profile
#::$(($(date +%s) + 43200))
export AWS_ACCESS_KEY_ID=$(echo $aws_creds | jq -r ".Credentials | .AccessKeyId") 
export AWS_SECRET_ACCESS_KEY=$(echo $aws_creds | jq -r ".Credentials | .SecretAccessKey") 
export AWS_SESSION_TOKEN=$(echo $aws_creds | jq -r ".Credentials | .SessionToken") 
EOF

    get_cached_aws_credentials
}
