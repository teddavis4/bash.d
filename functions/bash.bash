#!/usr/bin/env bash

function silent() {
    $@ 1>/dev/null 2>/dev/null
}

function quiet() {
    $@ 1>/dev/null
}

function happy() {
    $@ 2>/dev/null
}

function noerr() {
    $@ 2>&1
}

function noout() {
    $@ 1>&2
}

function failed() (
    echo "[31m--------------------[0m"
    echo "[31m""${@-$(date)}""[0m"
    echo "[31m--------------------[0m"
    return 1
)

function success() (
    echo "[32m--------------------[0m"
    echo "[32m""${@-$(date)}""[0m"
    echo "[32m--------------------[0m"
    return 0
)

function summarize() {
    time "${@}" && success || failed
}

function reload() {
    exec bash -l
}

function deleet() {
    local tmpdel=$(mktemp -d)
    rsync -a --delete "${tmpdel}/" "$*"
    rm -rf "${tmpdel}"
}

# Config
alias dirs='dirs -v'

# BASH_D
alias new_override="${EDITOR:-code} ${BASH_D_DIR}/overrides.misc.bash"

# Convenience
alias cat=bat