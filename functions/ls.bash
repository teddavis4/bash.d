alias ll="ls -l"
alias l="ls -loghF"
alias lrt="ls -alhrt"
if [[ "$(uname -s)" == "Darwin" ]]; then
    export CLICOLOR=1
    export LSCOLORS="exfxbxdxCxegedabagacad"
else
    export LS_COLORS="di=34:ln=35:so=31:pi=33:ex=1;32:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43"
    alias ls="ls --color=auto"
fi
