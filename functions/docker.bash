#!/usr/bin/env bash

stop_docker() {
    test -z "$(docker ps -q 2>/dev/null)" && osascript -e 'quit app "Docker"'
}

start_docker() {
    open --background -a Docker
}

docker_stop_containers() {
    docker ps -q | xargs -L1 docker stop
}
