#!/bin/bash

eval "exec 151>>${BASH_D_LOG_DIR}/bash.d.log"

log_info() {
    local msg=$(get_msg_or_json $*)
    echo '{"date": "'$(date)'", "log_level": "INFO", "shell_pid": "'$$'", "message": '$msg'}' >&3
}
log_debug() {
    local msg=$(get_msg_or_json $*)
    echo '{"date": "'$(date)'", "log_level": "DEBUG", "shell_pid": "'$$'", "message": '$msg'}' >&4
}
log_error() {
    local msg=$(get_msg_or_json $*)
    echo '{"date": "'$(date)'", "log_level": "ERROR", "shell_pid": "'$$'", "message": '$msg'}' >&2
}
log_trace() {
    local msg=$(get_msg_or_json $*)
    echo '{"date": "'$(date)'", "log_level": "TRACE", "shell_pid": "'$$'", "message": '$msg'}' >&5
}

get_msg_or_json() {
    local msg=$*
    if [[ ${msg:0:1} == "{" ]]; then
        echo $msg
    else
        echo '"'${msg}'"'
    fi
}

for i in 1 2 3; do
    fd=$(expr 2 + $i)
    if [[ $BASH_D_VERBOSE -ge $i ]]; then
        eval "exec $fd>&151"
    else
        eval "exec $fd> /dev/null"
    fi
done
