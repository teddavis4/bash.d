if [[ ${BASH_D_LOGGING_ENABLED:-0} -gt 0 ]]; then
    source "${BASH_D_DIR}/helpers/logging.bash"
fi
if [[ ${BASH_D_PATH_ENABLED:-0} -gt 0 ]]; then
    source "${BASH_D_DIR}/helpers/path.bash"
fi
if [[ ${BASH_D_ITERM2_INTEG_ENABLED:-0} -gt 0 ]]; then
    log_trace "loading iterm2 shell integration"
    source "${BASH_D_DIR}/helpers/iterm2_shell_integration.bash"
fi
log_trace "loading git-prompt"
source "${BASH_D_DIR}/helpers/git-prompt.bash"
