#!/usr/bin/env bash

# Homebrew binary path lookup 
export PATH="/opt/homebrew/bin:/opt/homebrew/sbin:$PATH"

# YARN
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

# $HOME/.local/bin
if [[ -d ${HOME}/.local/bin ]]; then
    export PATH="${PATH}:${HOME}/.local/bin"
fi
