#!/bin/bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
bash_d_dir="$(cd ${script_dir} && cd .. && pwd)"

ADDED_FILES=0
OLD_DIR=$(pwd)
cd $HOME
for bash_file in $(find $bash_d_dir/dotfiles -maxdepth 1 -type f); do
    configured_bash_file=".$(basename ${bash_file})"
    if [[ -e "$configured_bash_file" && ! -L "$configured_bash_file" ]]; then
        echo "${configured_bash_file} already exists but is not a symbolic link."
        echo "moving ${configured_bash_file} to ${configured_bash_file}.bak"
        mv ${configured_bash_file} ${configured_bash_file}.bak
    fi
    if [[ ! -e "$configured_bash_file" ]]; then
        echo "adding configuration file: ${configured_bash_file}"
        ADDED_FILES=1
        ln -fs ${bash_file} ${configured_bash_file}
    fi
done
cd $OLD_DIR
if [[ $ADDED_FILES -gt 0 ]]; then
    echo "bash.d has been installed. By default it expects the bash.d installation to be at ${HOME}/.bash.d"
    echo "You can override this setting by setting BASH_D_DIR at the top of ${HOME}/.bashrc"
else
    echo "bash.d seems like it's already installed. If you are unsure you can run the following"
    echo "rm $HOME/."{$(cd $bash_d_dir/dotfiles && echo * | tr ' ' ',')}
fi
